import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import org.testng.annotations.Test;


public class YaposhkaTest {

    @Test
    public void pageHeader() {
        open("https://www.yaposhka.kh.ua/");
        $("#.header-secondary").shouldHave(text("Продукты"), text("Доставка и оплата"), text("О нас"), text("Бонусы"), text("Рестораны"));
        $("#search").shouldBe(visible);
        $("#a.not-logged-in").shouldBe(visible);
        $("#.desktop-logo").shouldBe(visible);
        $("#li.level0:nth-child(1)").hover().shouldHave(text("Сеты"));
    }

    @Test
    public void searchTest() {
        $("#search").click();

    }
}
